2.1 Для того, щоб додати нове домашне завдання в новий репозиторій є 2 способи:
Перший спосіб: 
- створюємо новий репозиторій на сайті gitlab.com, 
- клонуємо його на свій комп'ютер за допомогою команди git clone
- переміщюємо чи створюємо в папку файли, які потрібно запушити в gitlab
- далі командой git add вибираємо файли, які будемо комітити
- далі командой git commit -m комітимо файли
- далі командой git push пушемо файли в репозиторій

Другий спосіб:
- створюємо на комп'юторі папку (репозиторій)
- відкриваємо папку у програмі VCK
- додаємо/створюємо потрібні файли (проєкти)
- відкриваємо новий термінал
- командой git init створюємо новий репозиторій
- командой git add вибираємо файли, які будемо комітити
- командой git commit -m комітимо файли
- командой git remote rename origin називаємо репозитоорій
- створюємо новий репозиторій на сайті gitlab.com
- командой git remote add origin додаємо адресу репозиторія
- командой git push origin master пушемо файли до репозиторію

2.2 Для того, щоб додати існуюче домашне завдяння в новий репозиторій

- відкриваємо репозиторій на сайті gitlab.com з існуючим домашнім завданням, 
- створюємо папку у себе на комп'юторіу яку потрібно клонувати існуюче домашне завдання
- заходимо у папку і відкриваємо термінал
- командой git clone клонуємо існуюче домашне завдання собі на компьютер
- відкриваємо папку у програмі VCK
- відкриваємо новий термінал
- командой git init створюємо новий репозиторій
- командой git add вибираємо файли, які будемо комітити
- командой git commit -m комітимо файли
- командой git remote rename origin називаємо репозитоорій
- створюємо новий репозиторій на сайті gitlab.com
- командой git remote add origin додаємо адресу репозиторія
- командой git push origin master пушемо файли до репозиторію
